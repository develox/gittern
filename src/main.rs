use ansi_term::{Colour, Style};
use clap::{Arg, ArgAction, Command};
use log::debug;
use std::{env, process, str};

const NAME: &str = "gittern";
const VERSION: &str = "0.1.2";
const ABOUT: &str = "Git by patterns";
const AUTHOR: &str = "(c) 2021-2024 Develox Solutions";

fn main() {
    let matches = Command::new(NAME)
        .version(VERSION)
        .about(ABOUT)
        .author(AUTHOR)
        .arg(
            Arg::new("PATTERN")
                .help("Path patterns")
                .index(1)
                .action(ArgAction::Append)
                .global(true),
        )
        .arg(
            Arg::new("dir")
                .short('d')
                .long("dir")
                .help("Repository directory")
                .num_args(1)
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("verbose")
                .short('v')
                .long("verbose")
                .help("Show logs")
                .action(ArgAction::SetTrue),
        )
        .subcommand(Command::new("status").about("git status --porcelain"))
        .subcommand(Command::new("add").about("git add"))
        .subcommand(
            Command::new("skip")
                .about("git update-index --skip-worktree, defaults to skip modified files only")
                .arg(
                    Arg::new("all")
                        .short('a')
                        .long("all")
                        .help("skip all matched files in worktree")
                        .action(ArgAction::SetTrue),
                )
                .arg(
                    Arg::new("list")
                        .short('l')
                        .long("list")
                        .help("list")
                        .conflicts_with("modified")
                        .action(ArgAction::SetTrue),
                ),
        )
        .subcommand(
            Command::new("unskip")
                .about("git update-index --no-skip-worktree")
                .arg(
                    Arg::new("all")
                        .short('a')
                        .long("all")
                        .help("un-skip all files in worktree")
                        .conflicts_with("PATTERN")
                        .action(ArgAction::SetTrue),
                ),
        )
        .get_matches();

    let verbose_mode = matches.get_flag("verbose");
    if verbose_mode {
        println!("{} {}\n", NAME, VERSION);

        let mut log_level = String::from("info");
        for (key, value) in env::vars() {
            if key == "RUST_LOG" {
                log_level = value;
                break;
            }
        }

        unsafe {
            env::set_var("RUST_LOG", &log_level);
        }

        env_logger::init();

        debug!("Log level: {}", &log_level);
    } else {
        unsafe {
            env::set_var("RUST_LOG", "off");
        }
    }

    let subcommand = matches.subcommand_name().unwrap_or("status");
    debug!("subcommand: {}", subcommand);

    let current_dir = env::current_dir().unwrap().to_string_lossy().to_string();

    let repo_path = matches.get_one("dir").unwrap_or(&current_dir);

    if !gittern::is_repo(repo_path) {
        println!("{} is not a repo", repo_path);
        process::exit(1);
    }

    // see https://github.com/clap-rs/clap/blob/master/examples/git.rs
    let filters: Vec<&str> = matches
        .get_many::<String>("PATTERN")
        .into_iter()
        .flatten()
        .collect::<Vec<_>>()
        .iter()
        .map(|s| s.as_str())
        .collect();
    debug!("Filters: {:?}", filters);

    match matches.subcommand_name() {
        Some("add") => {
            if filters.is_empty() {
                println!("Pattern is missing.");
            } else {
                gittern::add(repo_path, &filters);
            }
        }

        Some("skip") => {
            if let Some(matches) = matches.subcommand_matches("skip") {
                if matches.get_flag("list") {
                    // list: pattern(s) is optional
                    let files = gittern::get_skipped(repo_path, &filters);
                    println!("Found {} skipped file(s).", files.len());
                    for path_path in files {
                        println!("- {}", path_path);
                    }
                } else if !filters.is_empty() {
                    // skip: pattern(s) is mandatory
                    // optionally, skip all matching files in worktree
                    // defaults to skip modified files only
                    let all = matches.get_flag("all");
                    gittern::skip(repo_path, &filters, all);
                }
            }
        }

        Some("unskip") => {
            if let Some(matches) = matches.subcommand_matches("unskip") {
                // optionally, unskip all files in worktree
                let all = matches.get_flag("all");
                gittern::unskip(repo_path, &filters, all);
            }
        }

        // defaults to "status"
        _ => {
            let statuses = gittern::get_status(repo_path, &filters);
            println!("Found {} change(s).", statuses.keys().len());

            for (path, (staged_status, unstaged_status)) in statuses {
                let flags = format!("{} {}", staged_status, unstaged_status);

                if staged_status == 'C' {
                    println!("{} {}", Colour::Red.paint(flags), Colour::Red.paint(path));
                } else if staged_status != ' ' {
                    println!(
                        "{} {}",
                        Colour::Green.paint(flags),
                        Colour::Green.paint(path)
                    );
                } else {
                    match unstaged_status {
                        'D' => println!(
                            "{} {}",
                            Style::new().dimmed().paint(flags),
                            Style::new().dimmed().paint(path)
                        ),
                        'M' => {
                            println!("{} {}", Colour::Blue.paint(flags), Colour::Blue.paint(path))
                        }
                        'N' => println!("{} {}", flags, path),
                        _ => println!("{} {}", flags, path),
                    }
                }
            }
        }
    }
}
