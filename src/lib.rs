use git2::{IndexEntryExtendedFlag, Repository, StatusOptions, StatusShow};
use log::warn;
use regex::RegexSet;
use std::collections::BTreeMap;
use std::path::Path;
use std::str;

type State = (char, char);

fn get_repo(dir: &str) -> Repository {
    Repository::discover(dir).expect("invalid repo")
}

pub fn is_repo(dir: &str) -> bool {
    Repository::discover(dir).is_ok()
}

#[test]
fn test_is_repo() {
    const TEST_DIR: &str = "../";
    assert!(!is_repo(TEST_DIR))
}

/// Shows status on matching files
///
/// # Arguments
///
/// * `dir` - The repo directory
/// * `patterns` - The file patterns
pub fn get_status(dir: &str, patterns: &Vec<&str>) -> BTreeMap<String, State> {
    let regex_set = RegexSet::new(patterns).expect("invalid pattern");

    let repo = get_repo(dir);

    let mut options = StatusOptions::new();
    options
        .show(StatusShow::IndexAndWorkdir)
        .include_untracked(true)
        .include_unmodified(false)
        .no_refresh(false);

    let statuses = repo.statuses(Some(&mut options)).unwrap();

    let mut files: BTreeMap<String, State> = BTreeMap::new();

    for entry in statuses.iter() {
        let path = entry.path().unwrap();

        if patterns.is_empty() || regex_set.is_match(path) {
            let status = entry.status();

            let staged_status = if status.is_conflicted() {
                // eg. WT_MODIFIED | CONFLICTED
                'C'
            } else if status.is_index_deleted() {
                // eg. INDEX_DELETED | WT_NEW
                'D'
            } else if status.is_index_modified() {
                // eg. INDEX_MODIFIED | WT_MODIFIED
                'M'
            } else if status.is_index_new() {
                'N'
            } else {
                ' '
            };

            let unstaged_status = if status.is_wt_deleted() {
                'D'
            } else if status.is_wt_modified() {
                'M'
            } else if status.is_wt_new() {
                'N'
            } else {
                ' '
            };

            if staged_status == ' ' && unstaged_status == ' ' {
                warn!("Unknown status: {:?}", status);
            }

            files.insert(String::from(path), (staged_status, unstaged_status));
        }
    }

    files
}

/// Lists modified files with given patterns
///
/// # Arguments
///
/// * `dir` - The repo directory
/// * `patterns` - The file patterns
pub fn get_modified(dir: &str) -> Vec<String> {
    let repo = get_repo(dir);

    let mut options = StatusOptions::new();
    options
        .show(StatusShow::IndexAndWorkdir)
        .include_unmodified(false)
        .no_refresh(false);

    let statuses = repo.statuses(Some(&mut options)).unwrap();

    let mut files: Vec<String> = Vec::new();

    for entry in statuses.iter() {
        let path = entry.path().unwrap();

        files.push(String::from(path));
    }

    files
}

/// Lists skipped files with given patterns
///
/// # Arguments
///
/// * `dir` - The repo directory
/// * `patterns` - The file patterns
pub fn get_skipped(dir: &str, patterns: &Vec<&str>) -> Vec<String> {
    let regex_set = RegexSet::new(patterns).expect("invalid pattern");

    let repo = get_repo(dir);

    let index = repo.index().expect("error reading index");

    let mut files: Vec<String> = Vec::new();

    for entry in index.iter() {
        let flags_ext =
            IndexEntryExtendedFlag::from_bits(entry.flags_extended).expect("invalid flags");

        if flags_ext.is_skip_worktree() {
            let path = str::from_utf8(&entry.path).unwrap();

            if patterns.is_empty() || regex_set.is_match(path) {
                files.push(String::from(path));
            }
        }
    }

    files
}

/// Stages matching files
///
/// # Arguments
///
/// * `dir` - The repo directory
/// * `patterns` - The file patterns
pub fn add(dir: &str, patterns: &Vec<&str>) -> bool {
    let regex_set = RegexSet::new(patterns).expect("invalid pattern");

    let repo = get_repo(dir);

    let index = repo.index().expect("error reading index");
    let mut new_index = repo.index().expect("error reading index");

    let changed: Vec<String> = get_status(dir, patterns).keys().cloned().collect();

    for entry in index.iter() {
        let path = str::from_utf8(&entry.path).unwrap();

        if regex_set.is_match(path) && changed.contains(&path.to_string()) {
            println!("Adding {}", path);
            new_index
                .add_path(Path::new(path))
                .expect("error adding to index");
        }
    }

    new_index.write().expect("error writing index");

    true
}

/// Skips matching files
///
/// # Arguments
///
/// * `dir` - The repo directory
/// * `patterns` - The file patterns
/// * `all` - False if only skipping modified files
pub fn skip(dir: &str, patterns: &Vec<&str>, all: bool) -> bool {
    let regex_set = RegexSet::new(patterns).expect("invalid pattern");

    let repo = get_repo(dir);

    let index = repo.index().expect("error reading index");
    let mut new_index = repo.index().expect("error reading index");

    let modified = if all { Vec::new() } else { get_modified(dir) };

    for mut entry in index.iter() {
        let path = str::from_utf8(&entry.path).unwrap();

        if regex_set.is_match(path) && (all || modified.contains(&path.to_string())) {
            let mut flags_ext =
                IndexEntryExtendedFlag::from_bits(entry.flags_extended).expect("invalid flags");

            if !flags_ext.is_skip_worktree() {
                println!("Skipping {}", path);
                flags_ext.set(IndexEntryExtendedFlag::SKIP_WORKTREE, true);
            }

            entry.flags_extended = flags_ext.bits();
            new_index.add(&entry).expect("error updating index entry");
        }
    }

    new_index.write().expect("error writing index");

    true
}

/// Un-skips matching files
///
/// # Arguments
///
/// * `dir` - The repo directory
/// * `patterns` - The file patterns
/// * `all` - True if un-skipping all files
pub fn unskip(dir: &str, patterns: &Vec<&str>, all: bool) -> bool {
    let regex_set = RegexSet::new(patterns).expect("invalid pattern");

    let repo = get_repo(dir);

    let index = repo.index().expect("error reading index");
    let mut new_index = repo.index().expect("error reading index");

    for mut entry in index.iter() {
        let path = str::from_utf8(&entry.path).unwrap();

        if all || regex_set.is_match(path) {
            let mut flags_ext =
                IndexEntryExtendedFlag::from_bits(entry.flags_extended).expect("invalid flags");

            if flags_ext.is_skip_worktree() {
                println!("Un-skipping {}", path);
                flags_ext.set(IndexEntryExtendedFlag::SKIP_WORKTREE, false);
            }

            entry.flags_extended = flags_ext.bits();
            new_index.add(&entry).expect("error updating index entry");
        }
    }

    new_index.write().expect("error writing index");

    true
}
