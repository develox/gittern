[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/develox/gittern)

# gittern

gittern is a command line tool for Git, written in Rust.

## Build

```sh
# add MUSL target
rustup target add x86_64-unknown-linux-musl

# build with MUSL target
cargo build --release --target x86_64-unknown-linux-musl --features vendored
```

### Dependencies:

- musl

## Usage

By default, gittern works in the current directory. Use -d or --dir to specify a different repo directory, for example:

```sh
gittern -d ~/workspace/hello
```

### List statuses filtered by patterns

Equivalent Git command:

```sh
git status | grep xml
```

gittern command:

```sh
gittern status xml
# status is the default action 
gittern xml
```

### Add files by patterns

Equivalent Git command:

```sh
git add a.xml
git add b.xml
...
# or
git ls-files [path] | grep '\.xml$' | xargs git add
```

gittern command:

```sh
gittern add '\.xml'
```

### Skip worktree by patterns

Equivalent Git command:

```sh
git update-index --skip-worktree a.xml
git update-index --skip-worktree b.xml
...
git update-index --skip-worktree path_a/a.yaml
git update-index --skip-worktree path_b/b.yaml
...
```

gittern command:

```sh
# skip files by patterns
gittern skip .xml .yaml

# list all skipped files
gittern skip -l

# list skipped files by patterns
gittern skip -l .yaml
```

### Un-skip worktree by patterns

Equivalent Git command:

```sh
git update-index --no-skip-worktree PATH
```

gittern command:

```sh
# unskip files by patterns
gittern unskip PATTERN_1 PATTERN_2 PATTERN_3 ...

# unskip all
gittern unskip -a
```
